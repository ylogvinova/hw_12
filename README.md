##1. Груминг

Мне нужно `хранить` посещения клиентов с собаками. Тетрадка с записями очень неудобно. Собак могут приводить `разные люди` ( то муж, то жена, то их ребенок приведет), поэтому людей мне хранить не надо. Но вот `контактные номера` их хранить надо. Еще я планирую `расширять список услуг`, так что их будет много. Так же нужно вести `учет за работниками`, которые специализируются только на определенных услугах. 
