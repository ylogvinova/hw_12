CREATE EXTENSION pgcrypto;

CREATE TABLE IF NOT EXISTS owners(
                                     uuid text PRIMARY KEY DEFAULT gen_random_uuid(),
                                     name varchar(255),
                                     phone integer NOT NULL
);

CREATE TABLE IF NOT EXISTS dog_breed(
                                        uuid text PRIMARY KEY DEFAULT gen_random_uuid(),
                                        name varchar(255) NOT NULL
);

CREATE TABLE IF NOT EXISTS dogs(
                                   uuid text PRIMARY KEY DEFAULT gen_random_uuid(),
                                   name varchar(255) NOT NULL,
                                   birth_date Date,
                                   dog_breed_uuid text REFERENCES dog_breed(uuid)
);

CREATE TABLE IF NOT EXISTS services(
                                       uuid text PRIMARY KEY DEFAULT gen_random_uuid(),
                                       name varchar(255) NOT NULL,
                                       price float NOT NULL
);

CREATE TABLE IF NOT EXISTS employees(
                                        uuid text PRIMARY KEY DEFAULT gen_random_uuid(),
                                        name varchar(255) NOT NULL,
                                        birth_date Date,
                                        salary float
);

CREATE TABLE IF NOT EXISTS employees_services (
                                                  id serial PRIMARY KEY,
                                                  employee_uuid text REFERENCES employees(uuid),
                                                  service_uuid text REFERENCES services(uuid)
);

CREATE TABLE IF NOT EXISTS  owners_dogs (
                                            id serial PRIMARY KEY,
                                            owner_uuid text REFERENCES owners(uuid),
                                            dog_uuid text REFERENCES dogs(uuid)
);

CREATE TABLE IF NOT EXISTS  registration_of_visit (
                                                      id serial PRIMARY KEY,
                                                      owner_uuid text REFERENCES owners(uuid),
                                                      dog_uuid text REFERENCES dogs(uuid),
                                                      service_uuid text REFERENCES services(uuid),
                                                      employee_uuid text REFERENCES employees(uuid),
                                                      visit_date Date
);

/*** START FUNCTIONS ***/
/** lets imagine we don't have duplicates **/

/* get breed uuid by name */
CREATE OR REPLACE FUNCTION get_dogs_breed(breed_name varchar(255)) RETURNS text
    LANGUAGE plpgsql AS
$$
BEGIN
    RETURN(SELECT uuid FROM dog_breed WHERE name = breed_name);
END
$$;

/* get owner uuid by phone */
CREATE OR REPLACE FUNCTION get_owner_by_number(owner_number integer) RETURNS text
    LANGUAGE plpgsql AS
$$
BEGIN
    RETURN(SELECT uuid FROM owners WHERE phone = owner_number);
END
$$;

/* get employee uuid by name */
CREATE OR REPLACE FUNCTION get_employee_by_name(employee_name varchar(255)) RETURNS text
    LANGUAGE plpgsql AS
$$
BEGIN
    RETURN(SELECT uuid FROM employees WHERE name = employee_name);
END
$$;

/* get dog uuid by name */
CREATE OR REPLACE FUNCTION get_dog_by_name(dog_name varchar(255)) RETURNS text
    LANGUAGE plpgsql AS
$$
BEGIN
    RETURN(SELECT uuid FROM dogs WHERE name = dog_name);
END
$$;

/* get service uuid by name */
CREATE OR REPLACE FUNCTION get_service_by_name(service_name varchar(255)) RETURNS text
    LANGUAGE plpgsql AS
$$
BEGIN
    RETURN(SELECT uuid FROM services WHERE name = service_name);
END
$$;

/* add new employee with a GOOOOD!!! salary*/
CREATE OR REPLACE FUNCTION add_new_employee()
    RETURNS trigger
    LANGUAGE plpgsql
AS
$$
BEGIN
    -- Check if new employee has name and salary
    IF (NEW.name = '') IS NOT FALSE THEN
        RAISE EXCEPTION 'employee name cannot be null';
    END IF;
    IF NEW.salary IS NULL THEN
        RAISE EXCEPTION '% cannot have null salary', NEW.name;
    END IF;

    -- If less 1000 do not allow add new employee
    IF NEW.salary < 1000 THEN
        RAISE EXCEPTION '% cannot have a small salary', NEW.name;
    END IF;
    RETURN NEW;
END;
$$;
/*** END FUNCTIONS ***/


/*** START PROCEDURES ***/
/* add new dog with breed */
CREATE OR REPLACE PROCEDURE add_new_dog(dog_name varchar(255), dog_birth_date Date, dog_breed varchar(255))
    LANGUAGE SQL AS
$$
INSERT INTO dogs(name, birth_date, dog_breed_uuid)
VALUES(dog_name, dog_birth_date, get_dogs_breed(dog_breed));
$$;
/* add relation dog with owners */
CREATE OR REPLACE PROCEDURE add_owners_to_dogs(owner_number integer, dog_name varchar(255))
    LANGUAGE SQL AS
$$
INSERT INTO owners_dogs(owner_uuid, dog_uuid)
VALUES(get_owner_by_number(owner_number), get_dog_by_name(dog_name));
$$;
/* add relation employee with service */
CREATE OR REPLACE PROCEDURE add_employee_to_service(employee_name varchar(255), service_name varchar(255))
    LANGUAGE SQL AS
$$
INSERT INTO employees_services(employee_uuid, service_uuid)
VALUES(get_employee_by_name(employee_name), get_service_by_name(service_name));
$$;
/* add new visit */
CREATE OR REPLACE PROCEDURE add_new_visit(owner_number integer, dog_name varchar(255), service_name varchar(255), employee_name varchar(255))
    LANGUAGE SQL AS
$$
INSERT INTO registration_of_visit(owner_uuid, dog_uuid, service_uuid, employee_uuid, visit_date)
VALUES(get_owner_by_number(owner_number), get_dog_by_name(dog_name), get_service_by_name(service_name), get_employee_by_name(employee_name), current_timestamp);
$$;
/*** END PROCEDURES ***/

/*** START TRIGGERS***/
CREATE TRIGGER add_employee BEFORE INSERT OR UPDATE ON employees
    FOR EACH ROW EXECUTE PROCEDURE add_new_employee();
/*** END TRIGGERS***/


/*** START INSERT DATA ***/

/* add dog breeds */
INSERT INTO dog_breed(name) VALUES ('pug'), ('jingo'), ('chin'), ('shiba');

/* add dogs */
CALL add_new_dog('Charley', '02-04-2020', 'shiba');
CALL add_new_dog('Jessy', '02-03-2020', 'pug');
CALL add_new_dog('Puppy', '02-01-2020', 'jingo');
CALL add_new_dog('Mikky', '01-04-2020', 'chin');
CALL add_new_dog('Bruno', '03-04-2020', 'shiba');

/* show dogs with breeds */
SELECT dogs.name, br.name AS breed_name FROM dogs
                                                 LEFT JOIN dog_breed AS br ON (br.uuid = dogs.dog_breed_uuid);

/* owners */
INSERT INTO owners(name, phone) VALUES
('Jim', 214743648),
('Ella', 214768648),
('Bill', 214763649),
('Chad', 214648364);

/* add owners to dogs */
CALL add_owners_to_dogs(214743648, 'Charley');
CALL add_owners_to_dogs(214743648, 'Jessy');
CALL add_owners_to_dogs(214768648, 'Puppy');
CALL add_owners_to_dogs(214763649, 'Mikky');
CALL add_owners_to_dogs(214648364, 'Bruno');

/* show owners and dogs */
SELECT d.name AS dog_name, o.name AS owner_name, o.phone AS owner_phone FROM owners_dogs
                                                                                 LEFT JOIN dogs AS d ON (d.uuid = owners_dogs.dog_uuid)
                                                                                 LEFT JOIN owners AS o ON (o.uuid = owners_dogs.owner_uuid);

/* services */
INSERT INTO services (name, price) VALUES
('wash', 100),
('cut', 200),
('clean', 250),
('hair removal', 300);

/* employees */
INSERT INTO employees (name, birth_date, salary) VALUES
('Alla', '02-11-1993', 2000),
('Sten', '04-12-1994', 2100),
('Aron', '11-11-1999', 2150);
/*insert incorrect salary or name*/
insert into employees (name, birth_date, salary) values
('Simon', '02-11-1993', 900);
insert into employees (name, birth_date, salary) values
('', '02-11-1993', 1200);

/* add service for employee*/
CALL add_employee_to_service('Alla', 'wash');
CALL add_employee_to_service('Sten', 'cut');
CALL add_employee_to_service('Sten', 'clean');
CALL add_employee_to_service('Aron', 'hair removal');

/* show employees with services*/
SELECT s.name AS service_name, e.name AS employee_name FROM employees_services
                                                                LEFT JOIN services AS s ON (s.uuid = employees_services.service_uuid)
                                                                LEFT JOIN employees AS e ON (e.uuid = employees_services.employee_uuid);

/* add new visits*/
CALL add_new_visit(214743648, 'Jessy', 'wash', 'Alla');
CALL add_new_visit(214768648, 'Puppy', 'clean', 'Sten');

/* show all visits*/
SELECT  o.name AS owner_name, o.phone AS owner_phone,
        d.name AS dog_name, e.name AS employee_name,
        s.name AS service_name, s.price AS service_price,
        rg.visit_date
FROM registration_of_visit AS rg
         LEFT JOIN owners AS o ON (o.uuid = rg.owner_uuid)
         LEFT JOIN dogs AS d ON (d.uuid = rg.dog_uuid)
         LEFT JOIN services AS s ON (s.uuid = rg.service_uuid)
         LEFT JOIN employees AS e ON (e.uuid = rg.employee_uuid);




